import mongoose from 'mongoose';

const orderSchema = new mongoose.Schema({
  name: {
    type: String,
  },
  price: {
    type: Number,
  },
  quality: {
    type: Number,
  },
  create_date: {
    type: Date,
    default: Date.now,
  },
  update_date: {
    type: Date,
    default: Date.now,
  },
});
// Export Contact model
const OrderModel = mongoose.model('orders', orderSchema);

export default OrderModel;
