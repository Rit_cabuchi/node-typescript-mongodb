import express, { Express } from 'express';
import morgan from 'morgan';
import cors from 'cors';
import * as dotenv from 'dotenv';

import orderRouter from './routes/order.router';

const app: Express = express();

dotenv.config();

app.use(morgan('dev'));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

/** Routes */
app.use('/', orderRouter);

/** Error handling */
app.use((req, res) => {
  const error = new Error('not found');
  return res.status(404).json({
    message: error.message,
  });
});

export default app;
