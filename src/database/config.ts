import dotenv from 'dotenv';
dotenv.config();

export interface IDevelopmentConfig {
  username: string;
  password: string;
  database: string;
  portDB: string;
  host: string;
}
export interface IConfig {
  [development: string]: IDevelopmentConfig;
}
const development: IDevelopmentConfig = {
  username: process.env.DB_USERNAME ?? '',
  password: process.env.DB_PASSWORD ?? '',
  database: process.env.DB_DATABASE ?? '',
  portDB: process.env.DB_PORT ?? '',
  host: process.env.DB_HOST ?? '',
};

const config: IConfig = { development };
export default config;
