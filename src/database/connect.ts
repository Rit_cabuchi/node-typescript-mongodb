import mongoose, { ConnectOptions } from 'mongoose';
import dbConfig from './config';

const env: string = process.env.NODE_ENV || 'development';
const config = dbConfig[env];

async function connect() {
  const dbUrl = `mongodb://${config.username}:${config.password}@${config.host}:${config.portDB}/${config.database}`;
  try {
    await mongoose.connect(dbUrl, { useNewUrlParser: true } as ConnectOptions);
    console.log('DB connected');
  } catch (error) {
    console.log('Could not connect to db');
  }
}
export default connect;
