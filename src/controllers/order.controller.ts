import { Request, Response } from 'express';
import OrderModel from '../models/order.model';

interface Order {
  name: string;
  quality: number;
  price: number;
}

let orders = [
  {
    order_id: 'OR01',
    name: 'OrderONE',
    quality: 2,
    price: 2000,
    created: '12/12/2021',
  },
  {
    order_id: 'OR02',
    name: 'OrderONE',
    quality: 3,
    price: 2000,
    created: '12/12/2021',
  },
  {
    order_id: 'OR03',
    name: 'OrderONE',
    quality: 4,
    price: 2000,
    created: '12/12/2021',
  },
  {
    order_id: 'OR04',
    name: 'OrderONE',
    quality: 5,
    price: 2000,
    created: '12/12/2021',
  },
];
/** getting all orders */
export const getOrders = async (req: Request, res: Response) => {
  return res.status(200).json({
    message: 'Get Orders Success',
    data: orders,
  });
};

/** getting a single order */
export const getOrder = async (req: Request, res: Response) => {
  const id: string = req.params.id;
  return res.status(200).json({
    message: '',
    data: orders.find((item) => item.order_id === id),
  });
};

// updating a post
export const updateOrder = async (req: Request, res: Response) => {
  const id: string = req.params.id;
  const name: string = req.body.name ?? null;
  const price: number = req.body.price ?? null;
  const quality: number = req.body.quality ?? null;
  orders.map((item) => {
    if (item.order_id === id) {
      item.name = name;
      item.price = price;
      item.quality = quality;
    }
    return item;
  });
  return res.status(200).json({
    message: 'order updated successfully',
  });
};

// deleting a order
export const deleteOrder = async (req: Request, res: Response) => {
  const id: string = req.params.id;
  orders = orders.filter((item) => item.order_id !== id);
  return res.status(200).json({
    message: 'order deleted successfully',
  });
};

export const addOrder = async (req: Request, res: Response) => {
  const body: Order = req.body;
  orders.push({ ...body, order_id: `OR${orders.length + 1}`, created: '21/22/2011' });
  return res.status(200).json({
    message: 'order added successfully',
  });
};
/**Connect DB */
export const getOrdersM = async (req: Request, res: Response) => {
  const orders = await OrderModel.find();
  return res.status(200).json({
    message: 'order added successfully',
    data: orders,
  });
};

export const getOrderM = async (req: Request, res: Response) => {
  const id: string = req.params.id;
  const order = await OrderModel.findById(id).exec();
  return res.status(200).json({
    message: 'get order successfully',
    data: order,
  });
};

export const updateOrderM = async (req: Request, res: Response) => {
  const id: string = req.params.id;
  const name: string = req.body.name ?? null;
  const price: number = req.body.price ?? null;
  const quality: number = req.body.quality ?? null;
  const order = await OrderModel.findByIdAndUpdate(id, { $set: { name: name, price: price, quality: quality } });
  return res.status(200).json({
    message: 'updated order successfully',
    data: order,
  });
};

export const deleteOrderM = async (req: Request, res: Response) => {
  const id: string = req.params.id;
  const order = await OrderModel.findByIdAndDelete(id);
  return res.status(200).json({
    message: 'deleted order successfully',
    data: order,
  });
};

export const addOrderM = async (req: Request, res: Response) => {
  const body: Order = req.body;
  OrderModel.create(body, function (_err, order) {
    return res.status(200).json({
      message: 'added order successfully',
      data: order,
    });
  });
};
