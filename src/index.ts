import http from 'http';
import connect from './database/connect';
import app from './app';

/** Server */
const httpServer = http.createServer(app);
const PORT: string = process.env.PORT ?? '3000';
httpServer.listen(PORT, async () => {
  console.log(`The server is running on port ${PORT}`);
  await connect();
});
