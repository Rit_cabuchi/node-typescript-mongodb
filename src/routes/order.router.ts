import express from 'express';
import {
  getOrders,
  getOrder,
  updateOrder,
  deleteOrder,
  addOrder,
  getOrdersM,
  getOrderM,
  updateOrderM,
  deleteOrderM,
  addOrderM,
} from '../controllers/order.controller';

const router = express.Router();

router.get('/orders', getOrders);
router.get('/orders/:id', getOrder);
router.put('/orders/:id', updateOrder);
router.delete('/orders/:id', deleteOrder);
router.post('/orders', addOrder);

router.get('/ordersm', getOrdersM);
router.get('/ordersm/:id', getOrderM);
router.put('/ordersm/:id', updateOrderM);
router.delete('/ordersm/:id', deleteOrderM);
router.post('/ordersm', addOrderM);
export = router;
